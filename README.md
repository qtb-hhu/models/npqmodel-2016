# Model of non-photochemical quenching for plants

Modelbase v1.0 implementation of the model of non-photochemical quenching in plants by Matuszyńska et al. (2016)


## Reference 
Matuszyńska, A., Heidari, S., Jahns, P. & Ebenhöh, O. A mathematical model of non-photochemical quenching to study short-term light memory in plants. Biochimica et Biophysica Acta (BBA) - Bioenergetics 1857, 1860–1869 (2016).
